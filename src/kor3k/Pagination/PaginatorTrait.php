<?php

namespace kor3k\Pagination;

trait PaginatorTrait
{
    protected $callback;

    public function __construct( callable $callback = null )
    {
        $this->callback	=   $callback;
    }

    /**
     *
     * @param int $limit
     * @param int $offset
     * @return \ArrayIterator
     */
    public function getItems( $limit = null , $offset = null )
    {
        $this->setLimit( $limit );
        $this->setOffset( $offset );

        return $this->getIterator();
    }

    /**
     *
     * @return array
     */
    public function toArray()
    {
        return iterator_to_array( $this->getIterator() , true );
    }

    /**
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->getPageForOffset( $this->getOffset() );
    }

    /**
     *
     * @return int
     */
    public function getPages()
    {
        $pages	=   ceil( $this->count() / $this->getLimit() );
        return $pages;
    }

    /**
     *
     * @return array page# => offset
     */
    public function getPagesOffset()
    {
        $cnt	=   $this->getPages();
        $pages	=   array();

        for( $i = 1 ; $i <= $cnt ; $i++ )
        {
            $pages[$i]	=   $this->getOffsetForPage( $i );
        }

        return $pages;
    }

    /**
     *
     * @param int $page
     * @return int
     */
    public function getOffsetForPage( $page )
    {
        $this->sanitizePage( $page );

        $tmp	=   $page - 1;
        $offset	=   $tmp * $this->getLimit();

        return $offset;
    }

    /**
     *
     * @param int $offset
     * @return int
     */
    public function getPageForOffset( $offset )
    {
        $tmp	=   floor( $offset / $this->getLimit() );
        $page	=   $tmp + 1;

        $this->sanitizePage( $page );

        return $page;
    }

    protected function sanitizePage( &$page )
    {
        if( $page >= $this->getPages() )
        {
            $page   =  $this->getPages();
        }
        else if( $page < 1 )
        {
            $page   =	1;
        }
    }

    /**
     *
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->getLimit();
    }

    /**
     * apply item callback if set
     *
     * @param \Iterator $iterator
     * @return \Iterator
     */
    protected function applyCallback( \Iterator $iterator )
    {
        if( $callback = $this->callback )
        {
            foreach( $iterator as $key => $item )
            {
                $callback( $item );
            }

            $iterator->rewind();
        }

        return $iterator;
    }
}