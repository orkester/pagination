<?php

namespace kor3k\Pagination;

trait OffsetLimitPaginatorTrait
{
    /**
     * @var int
     */
    protected $offset;
    /**
     * @var int
     */
    protected $limit;

    /**
     *
     * @param int $offset
     * @return $this
     */
    public function setOffset( $offset )
    {
        $this->offset   =   $offset;
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     *
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     *
     * @param int $limit
     * @return $this
     */
    public function setLimit( $limit )
    {
        $this->limit    =   $limit;
        return $this;
    }
}