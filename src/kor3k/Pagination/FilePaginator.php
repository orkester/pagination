<?php

namespace kor3k\Pagination;

class FilePaginator implements PaginatorInterface
{
    /**
     * @var \SplFileObject
     */
    private $file;
    /**
     * @var int
     */
    private $lines;

    use OffsetLimitPaginatorTrait;
    use PaginatorTrait
    {
        __construct as private setCallback;
    }

    /**
     * FilePaginator constructor.
     *
     * you should set line count. otherwise the file will be count line by line, which is slow.
     *
     * @param               $file
     * @param callable|null $callback
     * @param int|null      $lines line count of a file
     */
    public function __construct( $file , callable $callback = null , $lines = null )
    {
        if( $file instanceof \SplFileInfo )
        {
            $file   =    $file->openFile( 'r' );
        }
        else if( !$file instanceof \SplFileObject )
        {
            $file   =   new \SplFileObject( $file , 'r' );
        }

        $file->setMaxLineLen( 4096 );

        if( !$lines )
        {
            $lines    =   0;

            while( !$file->eof() )
            {
                $ln =   $file->fgets();
                $lines++;
            }

            $file->rewind();
        }

        $this->file     =   $file;
        $this->lines    =   $lines;
        $this->setCallback( $callback );
    }

    public function getIterator()
    {
        $generator = function()
        {
            $this->file->seek( $this->getOffset() );

            for( $i = 0; $i < $this->getLimit() ; $i++ , $this->file->next() )
            {
                if( $this->file->eof() )
                    return;

                yield $this->file->current();
            }
        };

        return $generator();
    }

    public function count()
    {
        return $this->lines;
    }

}