<?php

namespace kor3k\Pagination;

class ArrayPaginator implements PaginatorInterface
{
    private $items;

    use OffsetLimitPaginatorTrait;
    use PaginatorTrait
    {
        __construct as private setCallback;
    }

    public function __construct( array $items , callable $callback = null  )
    {
        $this->items    =   $items;
        $this->setCallback( $callback );
    }


    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return $this->applyCallback(
            new \ArrayIterator(
                array_slice( $this->items , $this->getOffset() , $this->getLimit() , true )
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return count( $this->items );
    }
}