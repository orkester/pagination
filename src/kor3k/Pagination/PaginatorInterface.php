<?php

namespace kor3k\Pagination;

interface PaginatorInterface extends \Countable, \IteratorAggregate
{
    /**
     *
     * @param int $offset
     * @return $this
     */
    public function setOffset( $offset );
    /**
     *
     * @return int
     */
    public function getOffset();
    /**
     *
     * @return int
     */
    public function getLimit();
    /**
     *
     * @param int $limit
     * @return $this
     */
    public function setLimit( $limit );
    /**
     *
     * @return int
     */
    public function getPages();
    /**
     *
     * @return int
     */
    public function getCurrentPage();
    /**
     *
     * @return array
     */
    public function toArray();
    /**
     *
     * @param int $limit
     * @param int $offset
     * @return \ArrayIterator
     */
    public function getItems( $limit = null , $offset = null );
    /**
     *
     * @return array page# => offset
     */
    public function getPagesOffset();
    /**
     *
     * @param int $page
     * @return int
     */
    public function getOffsetForPage( $page );
    /**
     *
     * @param int $offset
     * @return int
     */
    public function getPageForOffset( $offset );
    /**
     *  alias for getLimit()
     *
     * @return int
     */
    public function getItemsPerPage();
}