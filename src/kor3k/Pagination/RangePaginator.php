<?php

namespace kor3k\Pagination;

class RangePaginator implements PaginatorInterface
{
    private $count;

    use OffsetLimitPaginatorTrait;
    use PaginatorTrait
    {
        __construct as private setCallback;
    }

    /**
     * RangePaginator constructor.
     *
     * @param int           $totalCnt
     * @param callable|null $callback
     */
    public function __construct( $totalCnt , callable $callback = null )
    {
        $this->count = $totalCnt;
        $this->setCallback( $callback );
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        $offset =   $this->getOffset();
        $offset =   $offset ?: 1;
        $limit  =   $offset + $this->getLimit();
        $limit  =   $limit > $this->count() ? $this->count() : $limit - 1;

        return $this->applyCallback(
            new \ArrayIterator(
                range( $offset , $limit )
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return $this->count;
    }
}